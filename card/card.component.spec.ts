import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { timer } from 'rxjs';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set shared=true when share button clicked', () => {
    const compiled = fixture.nativeElement;
    compiled.querySelector('.example-card #share-button').click();

    expect(component.shared).toBeTrue();
  });

  it('should disable the share button after it is clicked', () => {
    const compiled = fixture.nativeElement;
    const button: HTMLElement = compiled.querySelector('.example-card #share-button');

    button.click();

    fixture.detectChanges();

    const disabled = button.getAttributeNames().find(a => a === 'disabled');
    expect(disabled).toMatch('disabled');
  });

  it('should not increment likes above 5', () => {
    // 6 likes
    component.like();
    component.like();
    component.like();
    component.like();
    component.like();
    component.like();

    expect(component.likes).toBe(5);
  });

});
