import {MatCardModule} from '@angular/material/card';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  shared = false;
  liked = false;
  likes = 0;
  
  constructor() { }

  ngOnInit(): void {
  }

  share(){
    console.log('ay');
    this.shared = true;
  }

  like(){
    this.liked = true;
    if(this.likes < 5) this.incrementLikes();
  }

  incrementLikes(){
    this.likes++;
  }


}
