export interface Quote{
    success: {total : number};
    contents: {
        quotes: [{
            quote: string;
            length: string;
            author: string;
            tags: string[];
            category: string;
            date: string;
            permalink: string;
            title: string;
            background: string;
            id: string;
        }],
        copyright: string;
    };
}
