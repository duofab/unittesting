import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Quote } from './models/Quote';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {
  url = 'http://quotes.rest/qod.json';

  constructor(private http: HttpClient) { }

  getQuote(): Observable<Quote> {
    return this.http.get<Quote>(this.url);
  }
}1
