import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { QuoteService } from './quote.service';
import { HttpClient } from '@angular/common/http';
import { Quote } from './models/Quote';
import { Observable } from 'rxjs';

describe('QuoteService', () => {
  let service: QuoteService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [QuoteService, HttpClient]
    });
    service = TestBed.inject(QuoteService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getQuote() should return a single Observable<Quote>', ()=>{
    const quote: Quote = {
      success: {total : 1},
      contents: {
        quotes: [{
          quote: 'you mama so fat, they used her ass as an anker',
            length: '1000',
            author: 'u mama',
            tags: ['u', 'mama', 'fat'],
            category: 'bible',
            date: 'always',
            permalink: 'umamafat.com',
            title: 'u mama fat',
            background: 'no',
            id: 'fattest of dem all'
        }],
        copyright: 'u mama'
      }
    }

    service.getQuote().subscribe(q => {
      expect(q.contents.quotes.length).toBe(1);
      expect(q).toEqual(quote);
    })

    const request = httpTestingController.expectOne(service.url);
    expect(request.request.method).toBe('GET');
    request.flush(quote);
    httpTestingController.verify();
  })

});
